package kz.gep.xls_reader.services;

import kz.gep.xls_reader.model.Form103History;
import kz.gep.xls_reader.repo.Form103HistoryRepo;
import kz.gep.xls_reader.store.FTPDownloader;
import org.hibernate.boot.jaxb.SourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class HistoryBarcodeUpdator {

    @Autowired
    private Form103HistoryRepo form103HistoryRepo;

    @Autowired
    private XlsReadServiceImpl xlsReadService;

    @Autowired
    private FTPDownloaderService ftpDownloader;



    @PostConstruct
    public void updateHistory(){

        List<String> fileNamesList = new ArrayList<>();
        fileNamesList.add("postkzma14_160000_2018-06-19-100323.xls");
        int i = 0;
        int totCount = 0;
        for(String fileName : fileNamesList) {
            i++;
            System.out.println(i + ") WORKING WITH: " + fileName);
            //String fileName = "vsww_010000_2018-05-22-150011.xls";
            Map<String, String> map = xlsReadService.getMap(fileName);
            System.out.println("GOT Map : " + map.size());
            int countLetter = 0;
            int addCount = 0;
            for (Map.Entry<String, String> entry : map.entrySet()) {
                countLetter++;
                System.out.println(countLetter + " Count letter");

                List<Form103History> histories = null;
                Form103History history = null;
                System.out.println("Getting: " + entry.getValue());
                try {
                    histories = form103HistoryRepo.getVsHistoryList(entry.getValue());
                    System.out.println(histories.size());
                    if (histories.size() != 0) {
                        addCount++;
                        totCount++;
                        System.out.println("OOOOOOOOOOK will add " + addCount + " -- " + totCount);
                        history = histories.get(0);
                        history.setBarcode(entry.getKey());
                        form103HistoryRepo.save(history);
                    }
                } catch (Exception e) {
                    System.out.println("************************************************************Отсутствует в спиcке");
                    System.out.println(e.getLocalizedMessage());
                }
            }
        }

        System.out.println("DONE: ");
    }


}
