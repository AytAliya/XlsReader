package kz.gep.xls_reader.services;

import kz.gep.xls_reader.model.GeneralConfig;
import kz.gep.xls_reader.repo.GeneralConfigRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;


@Service
@Scope(scopeName = "singleton")
public class ConfigServiceImpl {
    @Autowired
    private GeneralConfigRepo generalConfigRepo;

    private List<GeneralConfig> ftpConfigList;

    @PostConstruct
    public void initFtpConfig() {
        ftpConfigList = generalConfigRepo.findAll();
    }

    public GeneralConfig getFtpConfigByList(int id) {
        GeneralConfig ftpConfig = null;
        if (ftpConfigList != null) {
            ftpConfig = ftpConfigList.stream().filter(config -> config.getId() == id).findFirst().orElse(null);
        }
        return ftpConfig;
    }

}
