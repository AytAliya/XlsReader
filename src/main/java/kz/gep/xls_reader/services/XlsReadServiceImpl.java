package kz.gep.xls_reader.services;

import kz.gep.xls_reader.model.xlsModels.Form103XlsCellBodyDescription;
import kz.gep.xls_reader.model.xlsModels.Form103XlsCellHeaderDescription;
import kz.gep.xls_reader.model.xlsModels.Form103XlsSheet;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Service
public class XlsReadServiceImpl {

    private static final String PATH = "src/main/resources/";

    public Map<String, String> getMap(String fileName){
        Map<String, String> finalMap = new LinkedHashMap<>();

        Form103XlsSheet form103XlsSheet = convertForm103Xls(fileName);
        for(Form103XlsCellBodyDescription body : form103XlsSheet.getForm103XlsCellBodyDescription()){
            finalMap.put(body.getBarcode(), body.getAddressee());
        }
        return finalMap;
    }


    private Form103XlsSheet convertForm103Xls(String name) {
        Form103XlsCellHeaderDescription header = new Form103XlsCellHeaderDescription();
        List<Form103XlsCellBodyDescription> bodyList = new ArrayList<>();
        Form103XlsSheet form103XlsSheet = new Form103XlsSheet();

        Workbook wb = null;
        try {
            wb = WorkbookFactory.create(new File(PATH + name));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        Sheet sheet = wb.getSheetAt(0);
        Row row;

        Iterator rows = sheet.rowIterator();

        row = (Row) rows.next();
        header.setDirection(row.getCell(1).getStringCellValue());

        row = (Row) rows.next();
        header.setTypeRegisteredMail(row.getCell(1).getStringCellValue());

        row = (Row) rows.next();
        header.setCategoryRegisteredMail(row.getCell(1).getStringCellValue());

        row = (Row) rows.next();
        header.setSender(row.getCell(1).getStringCellValue());

        if (row.getCell(3) != null) {
            header.setPhoneNumberFirstSender(row.getCell(3).getStringCellValue());
        }

        row = (Row) rows.next();
        header.setAppointmentsRegion(row.getCell(1).getStringCellValue());


        if (row.getCell(3) != null) {
            header.setPhoneNumberSecondSender(row.getCell(3).getStringCellValue());
        }

        row = (Row) rows.next();
        header.setIndexOPSPlace(row.getCell(1).getStringCellValue());


        if (row.getCell(3) != null) {
            header.setSenderEmail(row.getCell(3).getStringCellValue());
        }

        row = (Row) rows.next();
        header.setAllRegisteredMail(row.getCell(1).getColumnIndex());

        rows.next();

        while (rows.hasNext()) {
            Form103XlsCellBodyDescription body = new Form103XlsCellBodyDescription();

            row = (Row) rows.next();

            body.setAddressee(row.getCell(1).getStringCellValue());
            body.setBarcode(row.getCell(4).getStringCellValue());

            bodyList.add(body);
        }

        form103XlsSheet.setForm103XlsCellHeaderDescription(header);
        form103XlsSheet.setForm103XlsCellBodyDescription(bodyList);


        return form103XlsSheet;
    }

}
