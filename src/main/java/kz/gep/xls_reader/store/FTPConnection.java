package kz.gep.xls_reader.store;

import kz.gep.xls_reader.model.GeneralConfig;
import kz.gep.xls_reader.services.ConfigServiceImpl;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Scope(scopeName = "prototype")
public class FTPConnection {

    static {
        System.setProperty("java.net.preferIPv4Stack", "true");
    }

    private final Logger log = LogManager.getLogger(FTPConnection.class);

    private static final int MAIN_CONFIG_ID = 1;

    @Autowired
    private ConfigServiceImpl ftpConfigService;


    public void connect(FTPClient ftp) throws IOException {
        GeneralConfig ftpConf = ftpConfigService.getFtpConfigByList(MAIN_CONFIG_ID);
        ftp.connect(ftpConf.getFtpAddress(), Integer.parseInt(ftpConf.getFtpPort()));
        ftp.login(ftpConf.getFtpUser(), ftpConf.getFtpPassword());
        ftp.enterLocalPassiveMode();
        ftp.setFileType(FTP.BINARY_FILE_TYPE);
        int returnCode = ftp.getReplyCode();
        if (returnCode == 530) {
            log.error("FTP authorization error");
        }
    }

    public void disconnect(FTPClient ftp) {
        if (ftp.isConnected()) {
            try {
                ftp.logout();
                ftp.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
