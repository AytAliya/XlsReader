package kz.gep.xls_reader.store;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class FTPDownloader {

    private static final String PATH = "src/main/resources/";

    public void uploadFile(String ftpPathAndName, String getFileName, FTPClient ftpClient) {
        try {

            String remoteFilePath = ftpPathAndName;
            File localfile = new File(PATH + getFileName);

            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(localfile));
            ftpClient.retrieveFile(remoteFilePath, outputStream);
            outputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
