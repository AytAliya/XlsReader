package kz.gep.xls_reader.repo;

import kz.gep.xls_reader.model.Form103Arch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface Form103ArchRepo extends JpaRepository<Form103Arch, Long> {
    @Query("select f from Form103Arch f " +
            "where f.codePackage in ('3812MUYA5YSO2TV2DYZU6ZF10_4104196323')")
    List<Form103Arch> getGeneratingForm103Arch();
}
