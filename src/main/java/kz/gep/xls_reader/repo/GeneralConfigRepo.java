package kz.gep.xls_reader.repo;


import kz.gep.xls_reader.model.GeneralConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GeneralConfigRepo extends JpaRepository<GeneralConfig, Long> {

    @Query("select g.maxConvertPage from GeneralConfig g " +
            "where g.id = ?1")
    int getMaxConvertPage(long id);
}
