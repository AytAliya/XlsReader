package kz.gep.xls_reader.repo;


import kz.gep.xls_reader.model.Form103;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Form103Repo extends JpaRepository<Form103, Long> {



    @Query("select f from Form103 f " +
            "where f.codePackage in(:codePackage)")
    List<Form103> getGeneratingForm103(@Param("codePackage") List<String> list);



}