package kz.gep.xls_reader.repo;

import kz.gep.xls_reader.model.Form103History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Form103HistoryRepo extends JpaRepository<Form103History, Long> {

    @Query(value = "select * from z_form103_history where sender_id = 1 and id_type_doc = 1 " +
            //" and code_package not in (select code_package from z_form103_arch where sender_id = 1 and id_type_doc = 1 and sys_date > to_date('01.05.2018','dd.mm.yyyy')) " +
            "and sys_date > to_date('01.05.2018 15:00:00', 'dd.mm.yyyy hh24:mi:ss') " +
            "and sys_date < to_date('10.05.2018 18:00:00', 'dd.mm.yyyy hh24:mi:ss') " +
            " and f5 like :f5 " +
            " and f1 = :f1 " +
            " order by sys_date asc "
            , nativeQuery = true)
    List<Form103History> getHistoryList(@Param("f5") String f5, @Param("f1") String f1);


    @Query(value = "select * from z_form103_history where sender_id = 361 " +
            " and sys_date > to_date('18.06.2018', 'dd.mm.yyyy') " +
            " and sys_date < to_date('19.06.2018', 'dd.mm.yyyy') " +
            " and f5 like '1600%' " +
            " and f1 = :f1 " +
            " and code_package in ('2a835304db8e4aba9330df4c36966748',\n" +
            "'9b9a2ccef21e455f92d030eb980e8019',\n" +
            "'b360c5fc8a17439f9a28ac906f1b8199',\n" +
            "'3ec660d067764d8db6ef18aa441fdd5f',\n" +
            "'60a5975a24454514a4aa03e25224648b',\n" +
            "'4f549cf196d4444b8dcadf34bf1f2dce',\n" +
            "'10b2336c8a23439ab1d4a2070a658c1a',\n" +
            "'3e87aecc471740c2846064a609dbc2ba',\n" +
            "'08cbf04c142849528220b4f55984e38e',\n" +
            "'85c3ce7ec5a94720a7c730eb19c737a8',\n" +
            "'3e378971a32c4920aff23653f56a59c7',\n" +
            "'a935a6d8da394f00951f5252e9a80702',\n" +
            "'9654d7ac2c5e42fe95d814c679efc788',\n" +
            "'aa8ef8daeb15475d86335c330fbe30a1',\n" +
            "'22d7784ba29e4e478f360670c6c8c12a',\n" +
            "'10702a4aacac4be3bb87ce900bd17569',\n" +
            "'6eef95d996e64c5b8079e0fff000a357',\n" +
            "'0c229ae7b8204c2b938795afda67d02e',\n" +
            "'f01a25c53a7c41bc850eb9cf72d3f851',\n" +
            "'9227df41955b4f8992b4d60417cd509e',\n" +
            "'cf0ebf10339648598f3677127e9d025d',\n" +
            "'30b8593f380f4b5d95b73aca71f5d12a',\n" +
            "'16dc7b07c46542fea30fe00a1a3b9097',\n" +
            "'253c0bfba5494ee8815dce0a73a08d74',\n" +
            "'ab9e65a63e844451af4465021a748017',\n" +
            "'6d97b58e10074f7e8f0ac60be5f3cbb1',\n" +
            "'15d26f41f54f409a8a349bd7abaa662e',\n" +
            "'8ae53f94c37b478f8e34f8940b0c592d',\n" +
            "'3cf3683ba45949a89ad448d8951b8c11',\n" +
            "'1efc32c19ad042c6899ecaf382729653',\n" +
            "'7600dad35ac048129807680ffcef2901',\n" +
            "'99d8ea016d1c4572ac57302b2754b311',\n" +
            "'14107905f5c940ebb4f5b22152826f08',\n" +
            "'4ca251aff50243baa91da39b71fb0448',\n" +
            "'13ffeb95067c4b8fbd9a0e10a6d742ad',\n" +
            "'3a788b0340e04c15a3c0208c0109330b',\n" +
            "'8c879751153a47469f4b4d9ea024ff30',\n" +
            "'e9e104b3007849cd89121263bfc58c5f',\n" +
            "'4dfeae562eb2485c9a63cd90a206a571',\n" +
            "'40164d83e08c447bb07311d1109687f2',\n" +
            "'80c4689bb77d4a1c880cd2959e3a8934',\n" +
            "'6a00ceeae63d45d5b49a992c8ea9dcc4',\n" +
            "'fc13af688a4443d2a6dcba9e7d183d6f',\n" +
            "'7b4c38a231bd49978443b5282aa73e1e',\n" +
            "'15401440a707494b93220f7173482a4b',\n" +
            "'029fb6ba4db84fd0a40d4e177f26ab61',\n" +
            "'c9a4dca832bc4babae532db89ff996c0',\n" +
            "'5d69018db50748c58ce983b8392a8002',\n" +
            "'b842f19252ca41a5ad773a9afdbb5984',\n" +
            "'895b6fdc5bac4a76bf8f7b18b1ff3b61',\n" +
            "'843a85dd77af47e4a8ae7493e6935d16',\n" +
            "'f40ba98cf602485fa345a4e7e28affe6',\n" +
            "'b06fa6527e064caab24c27de25072966',\n" +
            "'47cdf913104141548aee505513a99efb',\n" +
            "'f712f81d39314736b14b1bf041c71d7b',\n" +
            "'27f65482ad534bb7a5bd18bfd6f204cb',\n" +
            "'1f236b32818e4291869b142c0d4c1fae',\n" +
            "'255dba6192fd4d149d183be28813a498',\n" +
            "'6f8c3a9268274e8ebd0ee3df83a5090a',\n" +
            "'55eeaf3d53b44569a858c3944a6217a7',\n" +
            "'c8f192cf4b064a5d927475af989c0e1c',\n" +
            "'c96ad67ee08441e2b4a31c50e23e9dc2',\n" +
            "'f087e3b2a0c44b5a8d16461c563a9ccc',\n" +
            "'c9777f61a9f8424dba404329b4b05da6',\n" +
            "'ec7a337ac1d8400eb49de64049d97e10',\n" +
            "'1a89bdc266eb4d8993698b62a7181e06',\n" +
            "'0056a807cc05467bb20a80b99c99cb39',\n" +
            "'59657fb34724436b9d3ab56af95804eb',\n" +
            "'bf0ccbb1a6794e8a8d73ba053fb0b583',\n" +
            "'c118292301934274b98dde2bbf39a803',\n" +
            "'c503c71ec55047b6a8eabcf6233252b0',\n" +
            "'295da13ba7fe49b388ed0f1bcdd16683',\n" +
            "'5f310dc46f154ee0b29b145c01d1ba1b',\n" +
            "'e896e57ab26147afa5f82d7643ffff22',\n" +
            "'131055196e1f4b1d8b229d91181274a6',\n" +
            "'584efbbebcdc40aeacb6853dc875635b',\n" +
            "'f30a84a3171a488bb82879e2627834ea',\n" +
            "'9c810a2684d941178cc1515cf2e10de0',\n" +
            "'76d6661ba86d4950868d1627ff414840',\n" +
            "'c2d66373e70e493786c0c435bcad1d69',\n" +
            "'e4afcd45c16d4875887156fa6287f5c4',\n" +
            "'4df1eb9adfe14c74988e6f47037eb8dc',\n" +
            "'827db41828ad435fa6f61a084fd74d30',\n" +
            "'8d206c6ed6df47d7ac04db56072e111b',\n" +
            "'6544d5e5f5a44fa796d1ec93b3a34339',\n" +
            "'ae9ff7a39ad343f884dd80b509b14e6b',\n" +
            "'c9db2dfdad7f4f0eafb9e05858dde0f1',\n" +
            "'38998755d3e843aa8f42d8087a7fcd9c',\n" +
            "'af64a942ffae4999ae4db9c1a232b878',\n" +
            "'8d7f92c2f24d42418c4faedc6160773c',\n" +
            "'44993e9145b24d83a8f518351b831738',\n" +
            "'9e1d44be06fe4452a224ba458dd3d2c0',\n" +
            "'a8cd58e0a1014c65985022f6bc2fbf97',\n" +
            "'202e06fbf33b40379a58cd63039877d8',\n" +
            "'cc17ebaeb69a417faf6c8544d45216c8',\n" +
            "'ce1789fd3fe5467c94187ccfe43060fc',\n" +
            "'fc6b1d3bf80b4519a98d758d99534786',\n" +
            "'33ab0d34d89a4576b94b0a596b8f293e',\n" +
            "'0e543e58f76c4f689c505d3035a87c3c',\n" +
            "'2cc350fb91a7440586da3986aa276fad',\n" +
            "'02956cf9c1b14126b723d8e90209d342',\n" +
            "'28525b364e064f53a74bcb0576d9a7c2',\n" +
            "'2372b064db3248e39c4c817f7c38f2bd',\n" +
            "'8564b8b7530b4b8d807c386e79c57c74',\n" +
            "'2575997ba5b64e4c838e37782df2809f',\n" +
            "'a520a8022684489880b14cc68b92c926',\n" +
            "'03142c039b844400a1b87d8ea8bde9c4',\n" +
            "'22dd11e7adcd4f808d6e7f2cb93878c3',\n" +
            "'d35ad279303641039acc73dd2272d128',\n" +
            "'ec1b18a2dfed4db083bfdf89940faf5d',\n" +
            "'2cc1d0e233e34f4f96aeb010dc75d313',\n" +
            "'46d4c480685f476e92b5cbb7f5babe1e',\n" +
            "'55aff6ea1952466d9631ee23c2303bb4',\n" +
            "'4bdb5c864e31401990cc37c9a7de6619',\n" +
            "'f862a3fcdeca42ddb1c5e87249b80658',\n" +
            "'1cc89678da4d4aeca4ba3c4304edb5ac',\n" +
            "'264f2c61ac394b83920bae70ab9bc07b',\n" +
            "'7b0973be0b994f9ba6b3eb1d3b1991fd',\n" +
            "'cbd1723579d649ff8ecf15f5aa78fd0a',\n" +
            "'f37f936a016543f5b04d40f143038af9',\n" +
            "'396a0b63b8db4906b53e6bca5006d6aa',\n" +
            "'68021fb5433e4d72826d3d5cd0930bff',\n" +
            "'2426859db19749619124fa7a2706f900',\n" +
            "'82efcf74e975445298d245723454a0be',\n" +
            "'9342d6289ac648f1ad43fb00f3fc44e2',\n" +
            "'a658a259da3b479eb8bb54af615a58e4',\n" +
            "'5c08397a5fbe4e108fe239def1800184',\n" +
            "'6da7c669159f4f249b40217714569c49',\n" +
            "'3e661f8063c946ecaa858a5eb667d126',\n" +
            "'8230dfc3e53b4f418345022892dcfcae',\n" +
            "'88881ff5ced946e0bee3ed0176ce6d8d',\n" +
            "'0bf62bf721114193ba2bcc0ed2a5afe1',\n" +
            "'b40a04d815f6474eb87ae28492aab465',\n" +
            "'10db6cfa1f514ff3a31389c65566be1a',\n" +
            "'85ab2f2a920e47dc9fda609867ce2fed',\n" +
            "'bb70c4d6eb7c48e19bb41e17331c256a',\n" +
            "'843ae280c24044c2ac261a1b202cecd7',\n" +
            "'68b96373c1644ce6b32cf090fec67a0d',\n" +
            "'85cc7ea57e974a919f2efbb3052393b4',\n" +
            "'4189eb24e64e4dfdaa81cc0d70f8ad87',\n" +
            "'83864e774706488da0ffc39953190d64',\n" +
            "'7b5e7cded43e44db87b0597ede43c727',\n" +
            "'6648f8d882594ecdb227b8516968ef97',\n" +
            "'44f7dd688fbc4e01b8cb81e15272831c',\n" +
            "'198ec997aff7456badda485bb94b0d2c',\n" +
            "'301b2c9039ae4b04a922b7cf4b6ae0fa',\n" +
            "'2ce63bfccd2b4142a1d56cc731977442',\n" +
            "'bb26becee9a74f5c8d538b7e86e5625c',\n" +
            "'060a2074f7104c76a191273579e384d7',\n" +
            "'e36704145c8f4e6297af3e673599ae34')" +
            " order by sys_date asc "
            , nativeQuery = true)
    List<Form103History> getVsHistoryList( @Param("f1") String f1);

}
