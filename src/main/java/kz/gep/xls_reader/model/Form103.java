package kz.gep.xls_reader.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Z_FORM103")
public class Form103 implements Comparable<Form103>{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QQEQ233FF")
    @SequenceGenerator(name = "QQEQ233FF", sequenceName = "S_Z_FORM103", allocationSize = 1)
    @Column(name = "ID_PACKAGE")
    private Long idPackage;
    @Column(name = "CODE_PACKAGE")
    private String codePackage;
    private String barcode;
    private String f1;
    private String f2;
    private String f3;
    private String f4;
    private String f5;
    private String f6;
    private String f7;
    private String f8;
    private String f9;
    private String f10;
    private String f11;
    private String f12;
    private String f13;
    private String f14;
    private String f15;
    private String f16;
    private String f17;
    private String f18;
    private String f19;
    private String f20;
    private String f21;
    private String f22;
    private String f23;
    private String f24;
    private String f25;
    private String status;

    @Column(name = "SENDER_ID")
    private long senderId;
    @Column(name = "STAT_PROC")
    private String statProc;
    @Column(name = "STAT_SEND")
    private String statSend;
    @Column(name = "SEND_DATE")
    private Date sendDate;
    @Column(name = "CHANGE_DATE")
    private Date changeDate;
    @Column(name = "ID_TYPE_DOC")
    private long idTypeDoc;
    @Column(name = "SYS_DATE")
    private Date sysDate;

    private String lob1;

    @Column(name = "PDF_CREATED")
    private String pdfCreated;
    @Column(name = "XLS_CREATED")
    private String xlsCreated;
    @Column(name = "PDF_SENDED")
    private String pdfSended;
    @Column(name = "XLS_SENDED")
    private String xlsSended;
    @Column(name = "PDF_SEND_DATE")
    private Date pdfSendDate;
    @Column(name = "XLS_SEND_DATE")
    private Date xlsSendDate;
    @Column(name = "READYDOC_ID")
    private Long readyDocId;
    @Column(name = "PAGE_COUNT")
    private Integer pageCount;
    @Column(name = "F119_CREATED")
    private Integer f119Created;
    @Column(name = "F119_SENT")
    private Integer f119Sent;
    @Column(name = "RECIPIENT_PHONE")
    private String resipientPhone;


    public Long getIdPackage() {
        return idPackage;
    }

    public void setIdPackage(long idPackage) {
        this.idPackage = idPackage;
    }

    public String getCodePackage() {
        return codePackage;
    }

    public void setCodePackage(String codePackage) {
        this.codePackage = codePackage;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getF1() {
        return f1;
    }

    public void setF1(String f1) {
        this.f1 = f1;
    }

    public String getF6() {
        return f6;
    }

    public void setF6(String f6) {
        this.f6 = f6;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public String getF2() {
        return f2;
    }

    public void setF2(String f2) {
        this.f2 = f2;
    }

    public String getF3() {
        return f3;
    }

    public void setF3(String f3) {
        this.f3 = f3;
    }

    public String getF4() {
        return f4;
    }

    public void setF4(String f4) {
        this.f4 = f4;
    }

    public String getF5() {
        return f5;
    }

    public void setF5(String f5) {
        this.f5 = f5;
    }

    public String getF7() {
        return f7;
    }

    public void setF7(String f7) {
        this.f7 = f7;
    }

    public String getF8() {
        return f8;
    }

    public void setF8(String f8) {
        this.f8 = f8;
    }

    public long getReadyDocId() {
        return readyDocId;
    }

    public void setReadyDocId(long readyDocId) {
        this.readyDocId = readyDocId;
    }

    public String getF9() {
        return f9;
    }

    public void setF9(String f9) {
        this.f9 = f9;
    }

    public String getF10() {
        return f10;
    }

    public void setF10(String f10) {
        this.f10 = f10;
    }

    public String getF11() {
        return f11;
    }

    public void setF11(String f11) {
        this.f11 = f11;
    }

    public String getF12() {
        return f12;
    }

    public void setF12(String f12) {
        this.f12 = f12;
    }

    public String getF13() {
        return f13;
    }

    public void setF13(String f13) {
        this.f13 = f13;
    }

    public String getF14() {
        return f14;
    }

    public void setF14(String f14) {
        this.f14 = f14;
    }

    public String getF15() {
        return f15;
    }

    public void setF15(String f15) {
        this.f15 = f15;
    }

    public String getF16() {
        return f16;
    }

    public void setF16(String f16) {
        this.f16 = f16;
    }

    public String getF17() {
        return f17;
    }

    public void setF17(String f17) {
        this.f17 = f17;
    }

    public String getF18() {
        return f18;
    }

    public void setF18(String f18) {
        this.f18 = f18;
    }

    public String getF19() {
        return f19;
    }

    public void setF19(String f19) {
        this.f19 = f19;
    }

    public String getF20() {
        return f20;
    }

    public void setF20(String f20) {
        this.f20 = f20;
    }

    public String getF21() {
        return f21;
    }

    public void setF21(String f21) {
        this.f21 = f21;
    }

    public String getF22() {
        return f22;
    }

    public void setF22(String f22) {
        this.f22 = f22;
    }

    public String getF23() {
        return f23;
    }

    public void setF23(String f23) {
        this.f23 = f23;
    }

    public String getF24() {
        return f24;
    }

    public void setF24(String f24) {
        this.f24 = f24;
    }

    public String getF25() {
        return f25;
    }

    public void setF25(String f25) {
        this.f25 = f25;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatProc() {
        return statProc;
    }

    public void setStatProc(String statProc) {
        this.statProc = statProc;
    }

    public String getStatSend() {
        return statSend;
    }

    public void setStatSend(String statSend) {
        this.statSend = statSend;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public long getIdTypeDoc() {
        return idTypeDoc;
    }

    public void setIdTypeDoc(int idTypeDoc) {
        this.idTypeDoc = idTypeDoc;
    }

    public Date getSysDate() {
        return sysDate;
    }

    public void setSysDate(Date sysDate) {
        this.sysDate = sysDate;
    }

    public String getLob1() {
        return lob1;
    }

    public void setLob1(String lob1) {
        this.lob1 = lob1;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public String getPdfCreated() {
        return pdfCreated;
    }

    public void setPdfCreated(String pdfCreated) {
        this.pdfCreated = pdfCreated;
    }

    public String getXlsCreated() {
        return xlsCreated;
    }

    public void setXlsCreated(String xlsCreated) {
        this.xlsCreated = xlsCreated;
    }


    public String getPdfSended() {
        return pdfSended;
    }

    public void setPdfSended(String pdfSended) {
        this.pdfSended = pdfSended;
    }

    public String getXlsSended() {
        return xlsSended;
    }

    public void setXlsSended(String xlsSended) {
        this.xlsSended = xlsSended;
    }

    public Date getPdfSendDate() {
        return pdfSendDate;
    }

    public void setPdfSendDate(Date pdfSendDate) {
        this.pdfSendDate = pdfSendDate;
    }

    public Date getXlsSendDate() {
        return xlsSendDate;
    }

    public void setXlsSendDate(Date xlsSendDate) {
        this.xlsSendDate = xlsSendDate;
    }

    public void setReadyDocId(Long readyDocId) {
        this.readyDocId = readyDocId;
    }

    public Integer getF119Created() {
        return f119Created;
    }

    public void setF119Created(Integer f119Created) {
        this.f119Created = f119Created;
    }

    public Integer getF119Sent() {
        return f119Sent;
    }

    public void setF119Sent(Integer f119Sent) {
        this.f119Sent = f119Sent;
    }

    public String getResipientPhone() {
        return resipientPhone;
    }

    public void setResipientPhone(String resipientPhone) {
        this.resipientPhone = resipientPhone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Form103 form103 = (Form103) o;
        return idPackage == form103.idPackage &&
                senderId == form103.senderId &&
                idTypeDoc == form103.idTypeDoc &&
                Objects.equals(codePackage, form103.codePackage) &&
                Objects.equals(barcode, form103.barcode) &&
                Objects.equals(f1, form103.f1) &&
                Objects.equals(f2, form103.f2) &&
                Objects.equals(f3, form103.f3) &&
                Objects.equals(f4, form103.f4) &&
                Objects.equals(f5, form103.f5) &&
                Objects.equals(f6, form103.f6) &&
                Objects.equals(f7, form103.f7) &&
                Objects.equals(f8, form103.f8) &&
                Objects.equals(f9, form103.f9) &&
                Objects.equals(f10, form103.f10) &&
                Objects.equals(f11, form103.f11) &&
                Objects.equals(f12, form103.f12) &&
                Objects.equals(f13, form103.f13) &&
                Objects.equals(f14, form103.f14) &&
                Objects.equals(f15, form103.f15) &&
                Objects.equals(f16, form103.f16) &&
                Objects.equals(f17, form103.f17) &&
                Objects.equals(f18, form103.f18) &&
                Objects.equals(f19, form103.f19) &&
                Objects.equals(f20, form103.f20) &&
                Objects.equals(f21, form103.f21) &&
                Objects.equals(f22, form103.f22) &&
                Objects.equals(f23, form103.f23) &&
                Objects.equals(f24, form103.f24) &&
                Objects.equals(f25, form103.f25) &&
                Objects.equals(status, form103.status) &&
                Objects.equals(statProc, form103.statProc) &&
                Objects.equals(statSend, form103.statSend) &&
                Objects.equals(sendDate, form103.sendDate) &&
                Objects.equals(changeDate, form103.changeDate) &&
                Objects.equals(sysDate, form103.sysDate) &&
                Objects.equals(lob1, form103.lob1) &&
                Objects.equals(pdfCreated, form103.pdfCreated) &&
                Objects.equals(xlsCreated, form103.xlsCreated) &&
                Objects.equals(pdfSended, form103.pdfSended) &&
                Objects.equals(xlsSended, form103.xlsSended) &&
                Objects.equals(pdfSendDate, form103.pdfSendDate) &&
                Objects.equals(xlsSendDate, form103.xlsSendDate) &&
                Objects.equals(readyDocId, form103.readyDocId) &&
                Objects.equals(pageCount, form103.pageCount) &&
                Objects.equals(f119Created, form103.f119Created) &&
                Objects.equals(f119Sent, form103.f119Sent) &&
                Objects.equals(resipientPhone, form103.resipientPhone);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPackage, codePackage, barcode, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17, f18, f19, f20, f21, f22, f23, f24, f25, status, senderId, statProc, statSend, sendDate, changeDate, idTypeDoc, sysDate, lob1, pdfCreated, xlsCreated, pdfSended, xlsSended, pdfSendDate, xlsSendDate, readyDocId, pageCount, f119Created, f119Sent, resipientPhone);
    }

    @Override
    public int compareTo(Form103 o) {
        if (pageCount == o.pageCount)
            return 0;
        else if (pageCount > o.pageCount)
            return 1;
        else
            return -1;
    }
}
