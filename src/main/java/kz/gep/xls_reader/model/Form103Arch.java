package kz.gep.xls_reader.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Z_FORM103_ARCH")
public class Form103Arch {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QQEQ233FF")
    @SequenceGenerator(name = "QQEQ233FF", sequenceName = "S_Z_FORM103_ARCH", allocationSize = 1)
	private long idPackage;
	@Column(name="CODE_PACKAGE")
	private String codePackage;
	private String barcode;
	private String f1;
	private String f6;
	private String f2;
	private String f3;
	private String f4;
	private String f5;
	private String f7;
	private String f8;
	private String f9;
	private String f10;
	private String f11;
	private String f12;
	private String f13;
	private String f14;
	private String f15;
	private String f16;
	private String f17;
	private String f18;
	private String f19;
	private String f20;
	private String f21;
	private String f22;
	private String f23;
	private String f24;
	private String f25;
	@Column(name="SENDER_ID")
	private long senderId;
	private String status;
	@Column(name="STAT_PROC")
	private String statProc;
	@Column(name="STAT_SEND")
	private String statSend;
	@Column(name="SEND_DATE")
	private Date sendDate;
	@Column(name="CHANGE_DATE")
	private Date changeDate;
	@Column(name="ID_TYPE_DOC")
	private long idTypeDoc;
	@Column(name="SYS_DATE")
	private Date sysDate;
	@JsonIgnore

	private String lob1;

	@Column(name="PAGE_COUNT")
	private Integer pageCount;
	@Column(name = "RECIPIENT_PHONE")
	private String resipientPhone;

	@Column(name = "PDF_CREATED")
	private String pdfCreated;
	@Column(name = "XLS_CREATED")
	private String xlsCreated;
	@Column(name = "PDF_SENDED")
	private String pdfSended;
	@Column(name = "XLS_SENDED")
	private String xlsSended;
	@Column(name = "PDF_SEND_DATE")
	private Date pdfSendDate;
	@Column(name = "XLS_SEND_DATE")
	private Date xlsSendDate;
	@Column(name = "READYDOC_ID")
	private long readyDocId;

	@Column(name = "FORM103_IDPACKAGE")
	private Long form103IdPackage;

	public long getIdPackage() {
		return idPackage;
	}
	public void setIdPackage(long idPackage) {
		this.idPackage = idPackage;
	}
	public String getCodePackage() {
		return codePackage;
	}
	public void setCodePackage(String codePackage) {
		this.codePackage = codePackage;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getF1() {
		return f1;
	}
	public void setF1(String f1) {
		this.f1 = f1;
	}
	public String getF6() {
		return f6;
	}
	public void setF6(String f6) {
		this.f6 = f6;
	}
	public long getSenderId() {
		return senderId;
	}
	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}
	public String getF2() {
		return f2;
	}
	public void setF2(String f2) {
		this.f2 = f2;
	}
	public String getF3() {
		return f3;
	}
	public void setF3(String f3) {
		this.f3 = f3;
	}
	public String getF4() {
		return f4;
	}
	public void setF4(String f4) {
		this.f4 = f4;
	}
	public String getF5() {
		return f5;
	}
	public void setF5(String f5) {
		this.f5 = f5;
	}
	public String getF7() {
		return f7;
	}
	public void setF7(String f7) {
		this.f7 = f7;
	}
	public String getF8() {
		return f8;
	}
	public void setF8(String f8) {
		this.f8 = f8;
	}
	public String getF9() {
		return f9;
	}
	public void setF9(String f9) {
		this.f9 = f9;
	}
	public String getF10() {
		return f10;
	}
	public void setF10(String f10) {
		this.f10 = f10;
	}
	public String getF11() {
		return f11;
	}
	public void setF11(String f11) {
		this.f11 = f11;
	}
	public String getF12() {
		return f12;
	}
	public void setF12(String f12) {
		this.f12 = f12;
	}
	public String getF13() {
		return f13;
	}
	public void setF13(String f13) {
		this.f13 = f13;
	}
	public String getF14() {
		return f14;
	}
	public void setF14(String f14) {
		this.f14 = f14;
	}
	public String getF15() {
		return f15;
	}
	public void setF15(String f15) {
		this.f15 = f15;
	}
	public String getF16() {
		return f16;
	}
	public void setF16(String f16) {
		this.f16 = f16;
	}
	public String getF17() {
		return f17;
	}
	public void setF17(String f17) {
		this.f17 = f17;
	}
	public String getF18() {
		return f18;
	}
	public void setF18(String f18) {
		this.f18 = f18;
	}
	public String getF19() {
		return f19;
	}
	public void setF19(String f19) {
		this.f19 = f19;
	}
	public String getF20() {
		return f20;
	}
	public void setF20(String f20) {
		this.f20 = f20;
	}
	public String getF21() {
		return f21;
	}
	public void setF21(String f21) {
		this.f21 = f21;
	}
	public String getF22() {
		return f22;
	}
	public void setF22(String f22) {
		this.f22 = f22;
	}
	public String getF23() {
		return f23;
	}
	public void setF23(String f23) {
		this.f23 = f23;
	}
	public String getF24() {
		return f24;
	}
	public void setF24(String f24) {
		this.f24 = f24;
	}
	public String getF25() {
		return f25;
	}
	public void setF25(String f25) {
		this.f25 = f25;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatProc() {
		return statProc;
	}
	public void setStatProc(String statProc) {
		this.statProc = statProc;
	}
	public String getStatSend() {
		return statSend;
	}
	public void setStatSend(String statSend) {
		this.statSend = statSend;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public Date getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}
	public long getIdTypeDoc() {
		return idTypeDoc;
	}
	public void setIdTypeDoc(long idTypeDoc) {
		this.idTypeDoc = idTypeDoc;
	}
	public Date getSysDate() {
		return sysDate;
	}
	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}
	//Убрал getter так как нужно только сохранять поле, но не возвращать
	public void setLob1(String lob1) {
		this.lob1 = lob1;
	}
	public Integer getPageCount() {
		return pageCount;
	}
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public String getLob1() {
		return lob1;
	}

	public String getResipientPhone() {
		return resipientPhone;
	}

	public void setResipientPhone(String resipientPhone) {
		this.resipientPhone = resipientPhone;
	}

	public String getPdfCreated() {
		return pdfCreated;
	}

	public void setPdfCreated(String pdfCreated) {
		this.pdfCreated = pdfCreated;
	}

	public String getXlsCreated() {
		return xlsCreated;
	}

	public void setXlsCreated(String xlsCreated) {
		this.xlsCreated = xlsCreated;
	}

	public String getPdfSended() {
		return pdfSended;
	}

	public void setPdfSended(String pdfSended) {
		this.pdfSended = pdfSended;
	}

	public String getXlsSended() {
		return xlsSended;
	}

	public void setXlsSended(String xlsSended) {
		this.xlsSended = xlsSended;
	}

	public Date getPdfSendDate() {
		return pdfSendDate;
	}

	public void setPdfSendDate(Date pdfSendDate) {
		this.pdfSendDate = pdfSendDate;
	}

	public Date getXlsSendDate() {
		return xlsSendDate;
	}

	public void setXlsSendDate(Date xlsSendDate) {
		this.xlsSendDate = xlsSendDate;
	}

	public long getReadyDocId() {
		return readyDocId;
	}

	public void setReadyDocId(long readyDocId) {
		this.readyDocId = readyDocId;
	}

	public Long getForm103IdPackage() {
		return form103IdPackage;
	}

	public void setForm103IdPackage(Long form103IdPackage) {
		this.form103IdPackage = form103IdPackage;
	}
}
