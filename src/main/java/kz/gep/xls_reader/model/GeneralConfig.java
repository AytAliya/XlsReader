package kz.gep.xls_reader.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_GENERAL_CONFIG")
public class GeneralConfig {
	@Id
	private long id;
	@Column(name="MAX_PAGE")
	private Integer maxPage;
	@Column(name="MAX_CONVERT_PAGE")
	private Integer maxConvertPage;
	@Column(name="FTP_HOST")
	private String ftpAddress;
	@Column(name="FTP_USER")
	private String ftpUser;
	@Column(name="FTP_PASSWORD")
	private String ftpPassword;
	@Column(name="FTP_PORT")
	private String ftpPort;
	@Column(name="MERGE_URL")
	private String mergeUrl;
	@Column(name="TEMP_DIR_TO_SAVE")
	private String tempDirToSave;
	@Column(name="LAUNCH_TIME")
	private String launchTime;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFtpAddress() {
		return ftpAddress;
	}
	public void setFtpAddress(String ftpAddress) {
		this.ftpAddress = ftpAddress;
	}
	public String getFtpUser() {
		return ftpUser;
	}
	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}
	public String getFtpPassword() {
		return ftpPassword;
	}
	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}
	public Integer getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}
	public Integer getMaxConvertPage() {
		return maxConvertPage;
	}
	public void setMaxConvertPage(Integer maxConvertPage) {
		this.maxConvertPage = maxConvertPage;
	}
	public String getFtpPort() {
		return ftpPort;
	}
	public void setFtpPort(String ftpPort) {
		this.ftpPort = ftpPort;
	}
	public String getMergeUrl() {
		return mergeUrl;
	}
	public void setMergeUrl(String mergeUrl) {
		this.mergeUrl = mergeUrl;
	}
	public String getTempDirToSave() {
		return tempDirToSave;
	}
	public void setTempDirToSave(String tempDirToSave) {
		this.tempDirToSave = tempDirToSave;
	}
	public String getLaunchTime() {
		return launchTime;
	}
	public void setLaunchTime(String launchTime) {
		this.launchTime = launchTime;
	}
	
}
