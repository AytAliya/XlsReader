package kz.gep.xls_reader;

import kz.gep.xls_reader.services.XlsReadServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XlsReaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(XlsReaderApplication.class, args);
	}
}
