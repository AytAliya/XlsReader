package kz.gep.xls_reader;


import kz.gep.xls_reader.services.XlsReadServiceImpl;
import kz.gep.xls_reader.model.Form103History;
import kz.gep.xls_reader.repo.Form103HistoryRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class XlsReaderApplicationTests {

    @Autowired
    private Form103HistoryRepo form103HistoryRepo;

    @Test
    public void contextLoads() {
        List<Form103History> histories = form103HistoryRepo.getHistoryList("0100%", "ПРОКОПЕНКО ВИТАЛИЙ КОНСТАНТИНОВИЧ");
        Form103History history = histories.get(0);
        history.setBarcode("WW01000000KZ");

        form103HistoryRepo.save(history);
    }

	@Test
	public void checkRead() {
		XlsReadServiceImpl xlsReadService = new XlsReadServiceImpl();
		xlsReadService.getMap("test2.xls");
	}

}
